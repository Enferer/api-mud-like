package fr.univ.apimudlike.repository

import fr.univ.apimudlike.model.Direction
import fr.univ.apimudlike.model.Room
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.namedparam.SqlParameterSource
import org.springframework.jdbc.support.JdbcUtils
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import java.util.stream.Collectors
import kotlin.random.Random


fun roomMapper(resultSet: ResultSet, rsNum: Int): Room {
    val listDirection: ArrayList<Direction> = ArrayList();
    if(resultSet.getString("roo_idwest")!=null) listDirection.add(Direction.W)
    if(resultSet.getString("roo_idnorth")!=null) listDirection.add(Direction.N)
    if(resultSet.getString("roo_ideast")!=null) listDirection.add(Direction.E)
    if(resultSet.getString("roo_idsouth")!=null) listDirection.add(Direction.S)

    return Room(resultSet.getString("roo_id"), listDirection)
}

fun intMapper(resultSet: ResultSet, rsNum: Int): Int {
    return resultSet.getInt(0)
}

@Repository
@PropertySource("classpath:db/query/room.properties")
class RoomDao {

    @Value("\${sql.get.room}")
    lateinit var findRoom: String

    @Value("\${sql.get.entity.by.room.id}")
    lateinit var getEntityByRoomId: String

    @Value("\${sql.get.neighbour.room.id}")
    lateinit var getNeighbourRoom: String

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate


    fun getById(idRoom: Int): Room? {

        var params : HashMap<String, Any>  = HashMap()
        params["roo_id"] = idRoom

        val sqlParameterSource: SqlParameterSource = MapSqlParameterSource(params)

        val room : Room? = namedParameterJdbcTemplate.queryForObject(findRoom, sqlParameterSource, ::roomMapper);

        return room;

    }

    fun getEntityByRoomId(idRoom: Int): List<String>? {

        var params : HashMap<String, Any>  = HashMap()
        params["roo_id"] = idRoom

        val sqlParameterSource: SqlParameterSource = MapSqlParameterSource(params)
        val entityList = namedParameterJdbcTemplate.queryForList(getEntityByRoomId, sqlParameterSource, String::class.java);

        return entityList.stream().map { s -> s.toString() }.collect(Collectors.toList()) ;

    }

    fun getNeighbour(guid: String, direction: String): Int? {
        var params: HashMap<String, Any> = HashMap()
        params["ent_id"] = guid

        val sqlParameterSource: SqlParameterSource = MapSqlParameterSource(params)
        return namedParameterJdbcTemplate.queryForObject(getNeighbourRoom.replace("<directionId>",direction), sqlParameterSource, Int::class.java)
    }

}