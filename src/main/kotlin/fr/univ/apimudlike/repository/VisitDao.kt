package fr.univ.apimudlike.repository

import fr.univ.apimudlike.model.Entity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
@PropertySource("classpath:db/query/visit.properties")
class VisitDao {

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    @Value("\${sql.visit}")
    lateinit var visitRoom: String

    @Value("\${sql.insert.entity}")
    lateinit var insertEntity: String

    @Value("\${sql.get.by.entity.id}")
    lateinit var getByEntityId: String

    fun visit(userId: String, roomId: Int) {

        var params : HashMap<String, Any>  = HashMap()
        params["ent_id"] = userId
        params["roo_id"] = roomId

        namedParameterJdbcTemplate.update(visitRoom,MapSqlParameterSource(params))

    }

    fun insertEntity(userId: String, roomId: Int) {

        var params : HashMap<String, Any>  = HashMap()
        params["ent_id"] = userId
        params["roo_id"] = roomId

        namedParameterJdbcTemplate.update(insertEntity,MapSqlParameterSource(params))

    }

    fun getRoomByEntityId(entityId: String): Int? {
        var params : HashMap<String, Any> = HashMap()
        params["ent_id"] = entityId

        return namedParameterJdbcTemplate.query(getByEntityId,MapSqlParameterSource(params), {resultSet, i -> resultSet.getInt(1) }).firstOrNull();
    }

}