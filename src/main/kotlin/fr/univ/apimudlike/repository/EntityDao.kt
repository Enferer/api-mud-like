package fr.univ.apimudlike.repository

import fr.univ.apimudlike.exception.EntityNotFoundException
import fr.univ.apimudlike.model.Entity
import fr.univ.apimudlike.model.Type
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.namedparam.SqlParameterSource
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import kotlin.collections.HashMap

fun entityMapper(resultSet: ResultSet, rsNum: Int): Entity {

    val type: Type = if (resultSet.getBoolean(3)) Type.MONSTER else Type.JOUEUR;
    return Entity(resultSet.getString(1), resultSet.getInt(2), type);

}

@Repository
@PropertySource("classpath:db/query/user.properties")
class EntityDao{

    @Value("\${sql.create.user}")
    lateinit var insertEntity: String

    @Value("\${sql.get.entity.by.id}")
    lateinit var getEntityById: String

    @Value("\${sql.hit.entity}")
    lateinit var hitEntity: String

    @Autowired
    lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    fun create(entity: Entity): Entity? {

        val sqlParameterSource: SqlParameterSource = MapSqlParameterSource(this.getParam(entity))

        namedParameterJdbcTemplate.update(insertEntity,sqlParameterSource)
        return entity;

    }

    fun getEntityById(eid: String): Entity? {

        var params : HashMap<String, Any>  = HashMap()
        params["ent_id"] = eid

        try{
            return namedParameterJdbcTemplate.queryForObject(getEntityById, params, ::entityMapper)
        }catch (e: EmptyResultDataAccessException){
            return null
        }
    }

    fun hitEntity(eid: String): Int {
        var params : HashMap<String, Any> = HashMap()
        params["ent_id"] = eid

        val sqlParameterSource = MapSqlParameterSource(params);

        return namedParameterJdbcTemplate.update(hitEntity, sqlParameterSource)
    }

    private fun getParam(entity: Entity): HashMap<String, Any> {
        var params : HashMap<String, Any>  = HashMap()

        params["ent_id"] = entity.eid
        params["ent_life"] = entity.totalVie

        return params
    }


}