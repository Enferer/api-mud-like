package fr.univ.apimudlike.dto

data class EntityIdDto(val cible: String)