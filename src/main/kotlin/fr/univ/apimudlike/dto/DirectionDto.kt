package fr.univ.apimudlike.dto

data class DirectionDto(val direction: String)