package fr.univ.apimudlike.dto

import fr.univ.apimudlike.model.Entity
import fr.univ.apimudlike.model.Room
import fr.univ.apimudlike.model.Type
import fr.univ.apimudlike.service.DEFAULT_LIFE

fun entityMapper(entity: Entity?) : EntityExamineDto{
    return EntityExamineDto(entity?.eid,entity?.type,entity?.totalVie, DEFAULT_LIFE);
}

data class EntityExamineDto(val description: String?, var type: Type?, var vie: Int?, var totalVie: Int?)