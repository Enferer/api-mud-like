package fr.univ.apimudlike.controller

import fr.univ.apimudlike.exception.NoRoomException
import fr.univ.apimudlike.dto.*
import fr.univ.apimudlike.exception.EntityNotFoundException
import fr.univ.apimudlike.exception.FalseDirectionException
import fr.univ.apimudlike.model.Room
import fr.univ.apimudlike.service.RoomService
import fr.univ.apimudlike.service.EntityService
import fr.univ.apimudlike.service.VisitService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class UserController {

    @Autowired
    lateinit var entityService: EntityService

    @Autowired
    lateinit var roomService: RoomService

    @Autowired
    lateinit var visitService: VisitService

    @PostMapping("/connect")
    fun postConnect() : PlayerConnectDto?{
        val user = entityService.postConnection()
        user?.let {
            visitService.getEntityRoom(it.eid)?.let {
                user?.salle = roomService.getById(it);
            }
        }

        return PlayerConnectDto(user?.eid,user?.totalVie,user?.salle)
    }

    @GetMapping("/{guid}/regarder")
    fun getLook(@PathVariable("guid") guid: String) : ResponseEntity<Room?>{
        visitService.getEntityRoom(guid)?.let {
            return ResponseEntity.status(HttpStatus.OK).body(roomService.getById(it));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/{guid}/deplacement")
    fun move(@PathVariable("guid") guid: String, @RequestBody direction : DirectionDto): ResponseEntity<Room?> {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(entityService.moveEntity(guid, direction))
        } catch (noRoomException : NoRoomException){
            return ResponseEntity.status(409).build()
        }catch (falseDirection : FalseDirectionException){
            return ResponseEntity.status(400).build()
        }
    }


    @GetMapping("/{guid}/examiner/{eid}")
    fun getEntity(@PathVariable("guid") guid: String, @PathVariable("eid") eid: String) : ResponseEntity<EntityExamineDto?> {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(entityMapper(entityService.getEntityByid(eid)))
        }catch (entityNotFound: EntityNotFoundException){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
        }
    }

    @PostMapping("/{guid}/taper")
    fun hitEntity(@RequestBody entity: EntityIdDto): ResponseEntity<Any> {
      try{
          entityService.hitEntity(entity.cible)
      } catch (entityNotFound: EntityNotFoundException) {
          return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
      }
      return ResponseEntity.ok().build()
    }
}