package fr.univ.apimudlike

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApiMudLikeApplication

fun main(args: Array<String>) {
	runApplication<ApiMudLikeApplication>(*args)
}
