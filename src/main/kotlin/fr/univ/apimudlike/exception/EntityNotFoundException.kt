package fr.univ.apimudlike.exception

import java.lang.Exception

class EntityNotFoundException(message: String) : Exception(message)