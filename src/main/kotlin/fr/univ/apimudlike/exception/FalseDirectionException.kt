package fr.univ.apimudlike.exception

import java.lang.Exception

class FalseDirectionException(message: String) : Exception(message)