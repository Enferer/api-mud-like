package fr.univ.apimudlike.exception

import java.lang.Exception

class NoRoomException(message: String) : Exception(message)