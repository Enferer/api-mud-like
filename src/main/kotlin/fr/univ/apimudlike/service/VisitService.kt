package fr.univ.apimudlike.service

import fr.univ.apimudlike.repository.VisitDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
public class VisitService {

    @Autowired
    lateinit var visitDao: VisitDao

    fun getEntityRoom(entityId: String): Int? {
        return visitDao.getRoomByEntityId(entityId)
    }

}