package fr.univ.apimudlike.service;

import fr.univ.apimudlike.exception.NoRoomException
import fr.univ.apimudlike.dto.DirectionDto
import fr.univ.apimudlike.exception.EntityNotFoundException
import fr.univ.apimudlike.exception.FalseDirectionException
import fr.univ.apimudlike.model.*
import fr.univ.apimudlike.repository.EntityDao
import fr.univ.apimudlike.repository.RoomDao
import fr.univ.apimudlike.repository.VisitDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;
import java.util.*

val DEFAULT_LIFE: Int = 20;
val DEFAULT_ROOM: Int = 1;

@Service
public class EntityService {

    @Autowired
    lateinit var entityDao: EntityDao

    @Autowired
    lateinit var visitDao: VisitDao

    @Autowired
    lateinit var roomDao: RoomDao

    @Autowired
    lateinit var roomService: RoomService

    fun postConnection(): Entity? {

        val user: Entity? = entityDao.create(Entity(generateUUID().toString(), DEFAULT_LIFE, Type.JOUEUR))
        user?.let {
            visitDao.insertEntity(it.eid, DEFAULT_ROOM)
        }

        return user

    }

    fun getEntityByid(eid: String): Entity? {
        val entity = entityDao.getEntityById(eid)
        if(entity != null){
            return entity
        }
        throw EntityNotFoundException("Entity d'id "+ eid +" n'existe pas")
    }

    fun moveEntity(guid: String, directionDto : DirectionDto): Room? {
        val direction: String = when(directionDto.direction) {
            "W" -> "roo_idWest"
            "N" -> "roo_idNorth"
            "E" -> "roo_idEast"
            "S" -> "roo_idSouth"
            else -> throw FalseDirectionException(directionDto.direction + "n'est pas une direction valide")
        }

        val roomId :Int? = roomDao.getNeighbour(guid,direction);

        if(roomId != null) {
            visitDao.visit(guid, roomId)
            return roomService.getById(roomId)
        }else {
            throw NoRoomException("Aucune salle dans la direction :" + directionDto.direction)
        }
    }

    fun hitEntity(eid: String) {
        if(entityDao.hitEntity(eid) == 0 ) throw EntityNotFoundException("Entity d'id "+ eid +" n'existe pas")
    }

    private fun generateUUID(): UUID = UUID.randomUUID();

}