package fr.univ.apimudlike.service

import fr.univ.apimudlike.model.Room
import fr.univ.apimudlike.repository.RoomDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
public class RoomService {

    @Autowired
    lateinit var roomDao: RoomDao;

    fun getById(roomId: Int): Room? {
        val room = roomDao.getById(roomId)
        room?.entites = roomDao.getEntityByRoomId(roomId).orEmpty();

        return room;
    }

}