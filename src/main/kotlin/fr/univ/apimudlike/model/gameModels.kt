package fr.univ.apimudlike.model

data class Entity(
        var eid: String,
        var totalVie :Int,
        val type: Type){
    var salle: Room? = null
}

data class Room(
        val description: String,
        val passages: List<Direction>) {
    lateinit var entites: List<String>
}

data class Dungeon(val dungeon: Array<Array<Room>>)
