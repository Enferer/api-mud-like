package fr.univ.apimudlike.model

enum class Type {
    JOUEUR, MONSTER
}