package fr.univ.apimudlike.model

enum class Direction {
    N, S, W, E
}