drop table if exists t_user;

create table t_entity (
    ent_id          varchar             not null,
    ent_life        integer             not null default '10',
    ent_ismonster     boolean           not null default true,
    constraint pk_t_user primary key (ent_id)
);

create sequence seq_room;
create table t_room (
    roo_id          integer     not null default nextval('seq_room') ,
    roo_idWest      integer,
    roo_idNorth     integer,
    roo_idEast      integer,
    roo_idSouth     integer,
    constraint pk_t_room primary key (roo_id)
);

create table t_visit (
    ent_id          varchar         not null,
    roo_id          integer         not null,

    constraint pk_t_visit primary key(ent_id),
    constraint fk_entity_id foreign key (ent_id) references t_entity(ent_id),
    constraint fk_room_id foreign key (roo_id) references t_room(roo_id)
);