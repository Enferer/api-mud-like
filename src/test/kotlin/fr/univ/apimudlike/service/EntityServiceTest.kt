package fr.univ.apimudlike.service

import com.nhaarman.mockitokotlin2.*
import fr.univ.apimudlike.exception.NoRoomException
import fr.univ.apimudlike.dto.DirectionDto
import fr.univ.apimudlike.exception.FalseDirectionException
import fr.univ.apimudlike.model.Direction
import fr.univ.apimudlike.model.Entity
import fr.univ.apimudlike.model.Room
import fr.univ.apimudlike.model.Type
import fr.univ.apimudlike.repository.EntityDao
import fr.univ.apimudlike.repository.RoomDao
import fr.univ.apimudlike.repository.VisitDao
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class EntityServiceTest {

    val entity = Entity("eid",42, Type.JOUEUR)
    val room = Room("rooid", listOf(Direction.N))

    @InjectMocks
    lateinit var entityService: EntityService

    @Mock
    lateinit var entityDao: EntityDao

    @Mock
    lateinit var visitDao: VisitDao

    @Mock
    lateinit var roomDao: RoomDao

    @Mock
    lateinit var roomService: RoomService

    @Test fun shouldReturnNewUser() {
        whenever(entityDao.create(any())).thenReturn(entity)
        assertEquals(entity,entityService.postConnection())
    }

    @Test fun shouldCallEntityDaoCreateMethod() {
        entityService.postConnection()
        verify(entityDao, times(1)).create(any())
    }

    @Test fun shouldCallVisitDaoInsertEntityMethod() {
        whenever(entityDao.create(any())).thenReturn(entity)

        entityService.postConnection()

        verify(visitDao, times(1)).insertEntity(any(),any())
    }

    @Test fun shouldCallEntityDaoGetByIdWherGetEntity() {
        whenever(entityDao.getEntityById("test")).thenReturn(entity)
        entityService.getEntityByid("test")

        verify(entityDao, times(1)).getEntityById("test")
    }

    @Test fun shouldReturnNewRoomWhenDirectionWasCorrect() {
        whenever(roomDao.getNeighbour("eid", "roo_idSouth")).thenReturn(1)
        whenever(roomService.getById(1)).thenReturn(room)

        assertEquals(room,entityService.moveEntity(entity.eid, DirectionDto("S")))
    }

    @Test fun shouldThrowNoRoomExceptionWhenDirectionWasNotCorrect() {
        assertThrows<FalseDirectionException> {
            entityService.moveEntity(entity.eid, DirectionDto("incorrect"))
        }
    }

    @Test fun shouldThrowNoRoomExceptionWhenNoRoomInDirection() {
        whenever(roomDao.getNeighbour("eid", "roo_idNorth")).thenReturn(null)

        assertThrows<NoRoomException> {
            entityService.moveEntity(entity.eid, DirectionDto("N"))
        }
    }

    @Test fun shouldCallEntityDaoGetByIdWhenHitEntity() {
        whenever(entityDao.hitEntity("test")).thenReturn(1)
        entityService.hitEntity("test")

        verify(entityDao, times(1)).hitEntity("test")
    }

}
