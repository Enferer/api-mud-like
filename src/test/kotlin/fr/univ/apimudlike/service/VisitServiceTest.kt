package fr.univ.apimudlike.service

import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import fr.univ.apimudlike.repository.RoomDao
import fr.univ.apimudlike.repository.VisitDao
import junit.framework.Assert.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on

@SpringBootTest
class VisitServiceTest {

    @InjectMocks
    lateinit var visitService: VisitService

    @Mock
    lateinit var visitDao: VisitDao

    @Test fun shouldCallVisitDaoGetRoomByIdEntity(){
        whenever(visitDao.getRoomByEntityId("eid")).thenReturn(1)

        visitService.getEntityRoom("eid")

        verify(visitDao, times(1)).getRoomByEntityId("eid")
    }

    @Test fun shouldReturnRoomIdWhenEntityIdWasCorrect(){
        whenever(visitDao.getRoomByEntityId("eid")).thenReturn(1)

        assertEquals(1,visitService.getEntityRoom("eid"))
    }

}