package fr.univ.apimudlike.service

import com.nhaarman.mockitokotlin2.whenever
import fr.univ.apimudlike.model.Direction
import fr.univ.apimudlike.model.Room
import fr.univ.apimudlike.repository.RoomDao
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class RoomServiceTest {

    val room = Room("rooid", listOf(Direction.N))

    @InjectMocks
    lateinit var roomService : RoomService

    @Mock
    lateinit var roomDao: RoomDao

    @Test fun shouldReturnRoomWhenIdIsCorrect() {
        whenever(roomDao.getById(1)).thenReturn(this.room)

        assertEquals(room, roomService.getById(1))
    }

    @Test fun shouldReturnRoomWithEntityWhenIdIsCorrect() {
        whenever(roomDao.getById(1)).thenReturn(this.room)
        whenever(roomDao.getEntityByRoomId(1)).thenReturn(listOf("e1","e2"))

        assertEquals(2, roomService.getById(1)?.entites?.size)
        assertEquals("e1", roomService.getById(1)?.entites?.get(0))
    }

    @Test fun shouldReturnNullWhenEntityIdNotExist() {
        whenever(roomDao.getById(1)).thenReturn(null)

        assertNull(roomService.getById(1))
    }

    @Test fun shouldReturnEmptyListOfEntityWhenNoEntityInRoomIdNotExist() {
        whenever(roomDao.getById(1)).thenReturn(this.room)
        whenever(roomDao.getEntityByRoomId(1)).thenReturn(null)

        assertEquals(0,roomService.getById(1)?.entites?.size)
    }
}