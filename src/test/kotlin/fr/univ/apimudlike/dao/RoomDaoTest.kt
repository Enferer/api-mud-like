package fr.univ.apimudlike.dao

import fr.univ.apimudlike.repository.RoomDao
import org.junit.Assert.assertNull
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertEquals
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class RoomDaoTest {

    @Autowired
    lateinit var roomDao: RoomDao

    @Test fun shouldReturnRoomWhenRoomIdExist(){
        val room = roomDao.getById(1)

        assertNotNull(room)
    }

    @Test fun shouldReturnNullWhenRoomNotIdExist(){
        val room = roomDao.getById(100)

        assertNull(room)
    }

}