package fr.univ.apimudlike.dao

import fr.univ.apimudlike.model.Entity
import fr.univ.apimudlike.model.Type
import fr.univ.apimudlike.repository.EntityDao
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.springframework.boot.test.context.SpringBootTest
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import java.lang.NullPointerException

@SpringBootTest
class EntityDaoTest {

    val entity = Entity("eid",42, Type.JOUEUR)

    @Autowired
    lateinit var entityDao: EntityDao

    @Test fun shouldReturnEntityWhenIdExist() {
        val entity = entityDao.getEntityById("gobelin1")

        assertEquals( Type.MONSTER, entity?.type)
        assertEquals( "gobelin1", entity?.eid)
    }

    @Test fun shouldReturnNullWhenIdNotExist() {
        val entity = entityDao.getEntityById("falseId")

        assertNull(entity)
    }

    @Test fun shouldReturnNewEntity() {
        val entity = entityDao.create(entity)

        assertEquals(this.entity, entity)
    }

    @Test fun shouldReturn1WhenEntityExist(){
        val entityHit = entityDao.hitEntity("gobelin1")

        assertEquals(1,entityHit)
    }

    @Test fun shouldReturn0WhenEntityExist(){
        val entityHit = entityDao.hitEntity("falseId")

        assertEquals(0,entityHit)
    }

}