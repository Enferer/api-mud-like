package fr.univ.apimudlike.controller

import com.nhaarman.mockitokotlin2.*
import fr.univ.apimudlike.dto.DirectionDto
import fr.univ.apimudlike.dto.EntityIdDto
import fr.univ.apimudlike.exception.EntityNotFoundException
import fr.univ.apimudlike.exception.FalseDirectionException
import fr.univ.apimudlike.exception.NoRoomException
import fr.univ.apimudlike.model.Direction
import fr.univ.apimudlike.model.Entity
import fr.univ.apimudlike.model.Room
import fr.univ.apimudlike.model.Type
import fr.univ.apimudlike.service.EntityService
import fr.univ.apimudlike.service.RoomService
import fr.univ.apimudlike.service.VisitService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus

@SpringBootTest
class UserControllerTest {

    val entity = Entity("eid",20, Type.JOUEUR)
    val room = Room("rooid", listOf(Direction.N))

    @InjectMocks
    lateinit var userController: UserController

    @Mock
    lateinit var visitService: VisitService

    @Mock
    lateinit var roomService: RoomService

    @Mock
    lateinit var entityService: EntityService

    @Test fun shouldReturnNewUserEntity(){
        val entity = userController.postConnect();

        assertNotNull(entity)
    }

    @Test fun shouldReturnNewUserWithHisRoom(){
        whenever(entityService.postConnection()).thenReturn(entity)
        whenever(visitService.getEntityRoom(this.entity.eid)).thenReturn(1)
        whenever(roomService.getById(1)).thenReturn(room)

        val entity = userController.postConnect();

        assertNotNull(entity?.salle)
    }

    @Test fun shouldReturnNewUserWithTotalLife(){
        whenever(entityService.postConnection()).thenReturn(entity)
        whenever(roomService.getById(1)).thenReturn(room)

        val entity = userController.postConnect();

        assertNotNull(entity?.totalvie)
        assertEquals(20, entity?.totalvie)
    }


    @Test fun shouldReturnCurrentRoomWhenUserExist(){

        whenever(visitService.getEntityRoom(entity.eid)).thenReturn(1)
        whenever(roomService.getById(1)).thenReturn(room)

        val room = userController.getLook(entity.eid)

        assertEquals(this.room.description, room.body?.description)
    }

    @Test fun shouldReturn200WhenUserExist(){

        whenever(visitService.getEntityRoom(entity.eid)).thenReturn(1)
        whenever(roomService.getById(1)).thenReturn(room)

        val room = userController.getLook(entity.eid)

        assertEquals(HttpStatus.OK, room.statusCode)
    }

    @Test fun shouldReturn404WhenUserNotExist(){

        whenever(visitService.getEntityRoom(entity.eid)).thenReturn(null)
        whenever(roomService.getById(1)).thenReturn(null)

        val room = userController.getLook(entity.eid)

        assertEquals(HttpStatus.NOT_FOUND, room.statusCode)
    }

    @Test fun shouldReturnNewRoomWhenUserExist(){

        whenever(entityService.moveEntity(entity.eid, DirectionDto("direction"))).thenReturn(room)

        val room = userController.move(entity.eid, DirectionDto("direction"))

        assertEquals(this.room, room.body)
    }

    @Test fun shouldReturn409WhenNoRoomInDirection(){

        doAnswer { throw NoRoomException("error") }
                .whenever(entityService)
                .moveEntity(entity.eid, DirectionDto("direction"))

        val room = userController.move(entity.eid, DirectionDto("direction"))

        assertEquals(409, room.statusCode.value())
    }

    @Test fun shouldReturn400WhenDirectionWasIncorrect(){

        doAnswer { throw FalseDirectionException("error") }
                .whenever(entityService)
                .moveEntity(entity.eid, DirectionDto("direction"))

        val room = userController.move(entity.eid, DirectionDto("direction"))

        assertEquals(400, room.statusCode.value())
    }

    @Test fun shouldReturnEntityWhenEntityIdExist() {

        whenever(entityService.getEntityByid(entity.eid)).thenReturn(entity)

        val responseEntity = userController.getEntity("guid",entity.eid)

        assertNotNull(responseEntity)
        assertEquals(entity.eid,responseEntity.body?.description)

    }

    @Test fun shouldReturn404WhenEntityNotExist() {

        doAnswer { throw EntityNotFoundException("error") }
                .whenever(entityService)
                .getEntityByid(entity.eid)

        val responseEntity = userController.getEntity("guid",entity.eid)

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.statusCode)

    }

    @Test fun shouldReturn200WhenHitEntityExist(){
        val entityIdDto = EntityIdDto(entity.eid)

        doNothing().`when`(entityService).hitEntity(entity.eid)

        assertEquals(HttpStatus.OK, userController.hitEntity(entityIdDto).statusCode)
    }

    @Test fun shouldReturn404WhenHitEntityNotExist(){

        val entityIdDto = EntityIdDto("eid")

        doAnswer { throw EntityNotFoundException("error") }
                .whenever(entityService)
                .hitEntity(entity.eid)

        assertEquals(HttpStatus.NOT_FOUND, userController.hitEntity(entityIdDto).statusCode)
    }

}