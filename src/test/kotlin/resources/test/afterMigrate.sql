INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'gobelin1', 10, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'gobelin2', 10, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'gobelin3', 10, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'gobelin4', 10, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'ogre1', 20, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'ogre2', 20, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'ogre3', 20, true );
INSERT INTO t_entity (ent_id, ent_life, ent_ismonster) VALUES ( 'ogre4', 20, true );


insert into t_room (roo_id, roo_idwest, roo_idnorth, roo_ideast, roo_idsouth)
values (1,2,3,4,null ),
        (2,5,null,1,null ),
        (3,null,8,null,1 ),
        (4,1,null,11,null ),
        (5,6,7,null,2 ),
        (6,null,null,5,null ),
        (7,null,null,null,5 ),
        (8,10,null,9,3 ),
        (9,8,null,null,null ),
        (10,null,null,8,null ),
        (11,4,14,13,12 ),
        (12,null,11,null,null ),
        (13,11,null,null,null ),
        (14,null,null,null,11 );


insert into t_visit (ent_id,roo_id)
values  ('gobelin1',1),
        ('gobelin2',1),
        ('gobelin3',1),
        ('gobelin4',1),
        ('ogre1',1),
        ('ogre2',1),
        ('ogre3',1),
        ('ogre4',1);


